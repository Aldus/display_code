<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

$query = "CREATE TABLE `".TABLE_PREFIX."mod_display_code` (
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `section_id`    int(11) DEFAULT '0',
    `page_id`       int(11) DEFAULT '0',
    `title`         varchar(255) DEFAULT NULL,
    `description`   text,
    `see_also`      varchar(255) DEFAULT '',
    `source_type`   varchar(255) NOT NULL DEFAULT 'php',
    `source`        text,
    `caption`       text,
    `last_edit`     int(11) DEFAULT '0',
    `active`        int(11) DEFAULT '1',
    `html_type`     varchar(255) DEFAULT 'pre',
    `style`         varchar(255) DEFAULT 'default',
    `line_numbers`  int(11) DEFAULT '0',
    `line_start`    int(11) DEFAULT '1',
    `template`      varchar(255) DEFAULT 'view.lte',
    `group`         int(11) DEFAULT '0',
    `position`      int(11) DEFAULT '1',
    PRIMARY KEY (`id`) );
";

if( false === $database->simple_query( $query ) )
{
    $admin->print_error( $database->get_error() );
}

$query ="CREATE TABLE `".TABLE_PREFIX."mod_display_code_groups` (
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`          varchar(255) DEFAULT 'No name',
    `description`   text,
    `active`        int(11) DEFAULT '1',
    `parent`        int(11) DEFAULT '0',
    `position`      int(11) DEFAULT '1',
    PRIMARY KEY (`id`) );
";

if (false === $database->simple_query($query)) {
    $admin->print_error($database->get_error());
}
