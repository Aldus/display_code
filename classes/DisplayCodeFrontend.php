<?php

declare(strict_types = 1);

namespace display_code;

use LEPTON_database;
use lib_twig_box;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class DisplayCodeFrontend
{
    const SELECT_PREFIX = "SELECT * FROM";
    const PHP_SUFFIX = ".php#";
    const CSS_FILE_PATH = "/modules/display_code/css/frontend.css";

    static bool $bCSSOutput = false;

    /**
     *  Static function to display a listing of a given group via a valid group-id.
     *
     *  @param  int $iID            A valid group-id.
     *  @param  string $sTemplateName  An optional template-file name.
     *  @return string                 The generated source.
     */
    static public function displayGroupByID(int $iID, string $sTemplateName="default_group.lte"): string
    {
        $database = LEPTON_database::getInstance();
        
        $aGroupInfo = [];
        $result = $database->execute_query(
            self::SELECT_PREFIX . " `" . TABLE_PREFIX."mod_display_code_groups` WHERE `id` = " . $iID,
            true,
            $aGroupInfo,
            false
        );
        
        if( false === $result )
        {
            return $database->get_error();
        }
        
        $aAllEntries = [];
        $result = $database->execute_query(
            self::SELECT_PREFIX . " `" . TABLE_PREFIX . "mod_display_code` WHERE `group` = " . $iID . " ORDER BY `position`",
            true,
            $aAllEntries,
            true
        );
        
        if( false === $result )
        {
            return $database->get_error();
        }
        
        foreach($aAllEntries as &$ref)
        {
            $aPageInfo = [];
            $database->execute_query(
                "SELECT `link`,`page_title`,`menu_title` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".$ref['page_id'],
                true,
                $aPageInfo,
                false
            );
            $ref['link'] = LEPTON_URL.PAGES_DIRECTORY.$aPageInfo['link'] . self::PHP_SUFFIX . $ref['section_id'];
            $ref['page_title']  = $aPageInfo['page_title'];
            $ref['menu_title']  = $aPageInfo['menu_title'];
            
        }
        
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerPath( dirname(__DIR__)."/templates/frontend/glossary/", "display_code");
        
        $returnValue = $oTWIG->render(
            "@display_code/".$sTemplateName,
            [
                'groupInfo'     => $aGroupInfo,
                'allEntries'    => $aAllEntries,
                'language'      => DisplayCode::getInstance()->language
            ]
        );
        
        if( false === self::$bCSSOutput )
        {
            
            $returnValue .= $oTWIG->render(
                "@display_code/cssToHead.lte",
                [
                    "css_file" => self::CSS_FILE_PATH
                ]
            );
            
            self::$bCSSOutput = true;
        
        }
        return $returnValue;
    }

    /**
     *  Static function to display the entry by a given valid section-id.
     *
     *  @param  ?int $iSectionID    A valid section-id.
     *  @return string              The generated source.
     */
    static public function displayCodeBySection(int $iSectionID = NULL ): string
    {
        if( NULL === $iSectionID )
        {
            return "";
        }
        
        $database = LEPTON_database::getInstance();
        $aCodeInfo = [];
        $database->execute_query(
            self::SELECT_PREFIX . " `" . TABLE_PREFIX . "mod_display_code` WHERE `section_id`=" . $iSectionID,
            true,
            $aCodeInfo,
            false
        );
        
        if(empty($aCodeInfo))
        {
            return sprintf(
                DisplayCode::getInstance()->language["no_section_found"],
                $iSectionID
            );
        }
        
        //  prepare values
        $style = ( $aCodeInfo['style'] === "default") ? "" : $aCodeInfo['style'];

        if($aCodeInfo['source_type'] === "html")
        {
            $aCodeInfo['source'] = str_replace("<", "&lt;", $aCodeInfo['source']);
        }
        DisplayCode::colorSource( $aCodeInfo['source'] , $style, $aCodeInfo['source_type']);

        if($aCodeInfo['line_numbers'] === 1)
        {
            DisplayCode::linenumbers( $aCodeInfo['source'] );
        }

        $oDISPLAY_CODE = DisplayCode::getInstance();

        $aSeeAlso = [];
        $aTempList = explode( ",", $aCodeInfo['see_also'] );
        foreach($aTempList as $iTempId)
        {
            foreach($oDISPLAY_CODE->allEntries as $ref)
            {
                if($ref['id'] === $iTempId)
                {
                    $link = LEPTON_URL.PAGES_DIRECTORY.$ref['link'] . self::PHP_SUFFIX . SEC_ANCHOR . $ref['section_id'];
            
                    $aSeeAlso[] = [
                        'title' => $ref['title'],
                        'link'  => $link
                    ];
                    break;
                }
            }
        }
        
        // output
        $oTWIG = lib_twig_box::getInstance();

        $oTWIG->registerModule("display_code");
        
        $page_values = [
            'content'	=> $aCodeInfo,
            'see_also'  => $aSeeAlso,
            'language'  => &$oDISPLAY_CODE->language
        ];
        
        $returnValue = $oTWIG->render(
            "@display_code/frontend/".$aCodeInfo['template'],	//	template-filename
	        $page_values	//	template-data
        );

        if (false === self::$bCSSOutput)
        {      
            $returnValue .= $oTWIG->render(
                "@display_code/frontend/glossary/cssToHead.lte", // !
                [
                    "css_file" => self::CSS_FILE_PATH
                ]
            );
            
            self::$bCSSOutput = true;
        
        }
        return $returnValue;
    }

    /**
     *  Static function to display the entry by a given valid id.
     *
     *  @param  ?int $iID   A valid id.
     *  @return string      The generated source.
     */    
    static public function displayCodeByID(int $iID = NULL): string
    {
        if (NULL === $iID)
        {
            return "";
        }
        
        $database = LEPTON_database::getInstance();
        $aCodeInfo = [];
        $database->execute_query(
            self::SELECT_PREFIX . " `" . TABLE_PREFIX . "mod_display_code` WHERE `id`=" . $iID,
            true,
            $aCodeInfo,
            false
        );

        if (empty($aCodeInfo))
        {
            return sprintf(
                DisplayCode::getInstance()->language["no_id_match"],
                $iID
            );
        }
        
        //  prepare values
        $style = ($aCodeInfo['style'] == "default") ? "" : $aCodeInfo['style'];

        if($aCodeInfo['source_type'] == "html")
        {
            $aCodeInfo['source'] = str_replace("<", "&lt;", $aCodeInfo['source']);
        }

        DisplayCode::colorSource( $aCodeInfo['source'] , $style, $aCodeInfo['source_type']);


        {if ($aCodeInfo['line_numbers'] === 1)
            DisplayCode::linenumbers( $aCodeInfo['source'] );
        }

        $oDISPLAY_CODE = DisplayCode::getInstance();
        $aSeeAlso = [];
        $aTempList = explode( ",", $aCodeInfo['see_also'] );
        $aTempList = array_map(function($a){ return (int)$a;}, $aTempList);

        foreach ($aTempList as $iTempId)
        {
            foreach($oDISPLAY_CODE->allEntries as $ref)
            {
                if($ref['id'] === $iTempId)
                {
                    $link = LEPTON_URL.PAGES_DIRECTORY.$ref['link'].".php#".SEC_ANCHOR.$ref['section_id'];
            
                    $aSeeAlso[] = [
                        'title' => $ref['title'],
                        'link'  => $link
                    ];
                    break;
                }
            }
        }
        
        // output[2]
        $oTWIG = lib_twig_box::getInstance();

        $oTWIG->registerModule("display_code");
        
        $page_values = [
            'content'	=> $aCodeInfo,
            'see_also'  => $aSeeAlso,
            'language'  => &$oDISPLAY_CODE->language
        ];

        $returnValue = $oTWIG->render(
            "@display_code/frontend/".$aCodeInfo['template'],
	        $page_values
        );

        if (false === self::$bCSSOutput)
        {      
            $returnValue .= $oTWIG->render(
                "@display_code/frontend/glossary/cssToHead.lte", // !
                [
                    "css_file" => self::CSS_FILE_PATH
                ]
            );
            
            self::$bCSSOutput = true;
        
        }
        return $returnValue;
    }
}
