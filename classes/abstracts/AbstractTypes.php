<?php

declare(strict_types = 1);

namespace display_code\abstracts;


/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

abstract class AbstractTypes
{
    protected string $ci_version = "5.5.0";

    static ?object $instance = null;

    public array $aPattern = [];
    
    /**
     *  Return the instance of this class.
     *
     */
    public static function getInstance(): object
    {
        if (static::$instance === null)
        {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @param string $sSource
     * @param string $sCSSClassName
     * @return void
     */
    public function colorSource(string &$sSource="", string $sCSSClassName="" ): void
    {
        if (strlen($sCSSClassName) > 0)
        {
            $sCSSClassName = " " . $sCSSClassName;
        }

        foreach ($this->aPattern as $key => $pattern)
        {
            $sSource = preg_replace(
                $pattern,
                "<span class='" . $key . $sCSSClassName . "'>$1</span>",
                $sSource
            );
        }
    }
}
