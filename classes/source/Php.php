<?php

declare(strict_types = 1);

namespace display_code\source;

use display_code\abstracts\AbstractTypes;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class Php extends AbstractTypes
{
    static ?object $instance = null;

    public array $aPattern = [
        "comment"       => "/(\/\*.*\*\/)/sU",
        "commentline"   => "/(\/\/.*)/",
        "vars"          => "/(\\\$[\w\->]+)/",
        "str"           => "/([\"][^\"]+[\"])/i",
        "keywords"      => "/(true|false|null)/i",
        "lepton"        => "/(LEPTON_core|LEPTON_tools|LEPTON_database|LEPTON_handle|LEPTON_frontend|LEPTON_abstract)/i",
        "php_self"      => "/(new|array|array_push|array_pop|array_reverse|list|each|foreach|class |trait|extends|public|private|static|return|function|use )/i"
    ];

    /**
     *  For the comments inside php.
     * @param string $sSource
     * @param string $sCSSClassName
     * @return void
     */
    public function colorSource(string &$sSource="", string $sCSSClassName=""  ): void
    {
        parent::colorSource($sSource, $sCSSClassName);
        
        $pattern = "/<span class='comment'>(\/\*.*\*\/)<\/span>/sU";
        $aAllMatches = [];
        
        $result = preg_match_all(
            $pattern,
            $sSource,
            $aAllMatches,
            PREG_PATTERN_ORDER
        );
        
        if(($result !== false) && ($result > 0))
        {
            foreach($aAllMatches[1] as $found)
            {
                $newString = str_replace("\n", "</span>\n<span class='comment'>", $found);
                
                $sSource = str_replace( $found, $newString, $sSource );
            }
        }        
    }
}
