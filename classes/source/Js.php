<?php

declare(strict_types = 1);

namespace display_code\source;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class Js extends \abstracts\AbstractTypes
{
    static ?object $instance = null;

    public array $aPattern = [
        // "comment"   => "/(&lt;!--.*-->)/sU",
        "commentline"   => "/(\/\/.*)/",
        "tags"      => "/(&lt;[^!].*>)/sU",
        "js_self"   => "/(new|array|return|function)/i"
    ];

}
