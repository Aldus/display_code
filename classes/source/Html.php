<?php

declare(strict_types = 1);

namespace display_code\source;

use display_code\abstracts\AbstractTypes;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class Html extends AbstractTypes
{
    static ?object $instance = null;
    
    public array $aPattern = [
        "comment"   => "/(&lt;!--.*-->)/sU",
        "tags"      => "/(&lt;[^!].*>)/sU"
    ];

}