<?php

declare(strict_types = 1);

namespace display_code;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class DisplayCodeParser extends \DisplayCode
{
    public array $aPattern = [
        "class"     => "/[\n|\r][ |\t]{0,}class ([^{\n\r]*)/i",
        "function"  => "/(static|public){0,1} function[ ]([^{]{0,})/si",
        "arguments" => "/\((.*)\)/i"
    ];
    
    public array $aBuffer = [];
    
    private string $sSource = "";
    
    /**
     *  Internal instance of this class
     *
     */
    static $instance;
    
    /**
     *  Initialize the class.
     *
     */
    public function initialize()
    {
        // nothing here - required by parent abstract class
    }

    /**
     * @param string $sFilename
     * @return void
     */
    public function getFile(string $sFilename="" ): void
    {
        if (file_exists($sFilename))
        {
            $this->sSource = file_get_contents($sFilename);
            $this->getClass();
            $this->getMethods();
        }
    
    }
    
    private function getClass()
    {
        $aFounds = [];
        if(preg_match_all( $this->aPattern['class'], $this->sSource, $aFounds, PREG_SET_ORDER) )
        {
            $this->aBuffer['class']= $aFounds[0][1];
        }
    }
    
    private function getMethods()
    {
        $aFounds = [];
        $this->aBuffer['methods'] = [];

        if (preg_match_all($this->aPattern['function'], $this->sSource, $aFounds, PREG_SET_ORDER))
        {
            foreach ($aFounds as $aRef)
            {
                $aTempInfo = [
                    'access'    => $aRef[1],
                    'method'    => explode("(", $aRef[2])[0]
                ];

                // args
                $aArgs = [];
                if (preg_match_all($this->aPattern['arguments'], $aRef[2], $aArgs, PREG_SET_ORDER))
                {
                    $aAllArgs = [];

                    if (strlen($aArgs[0][1]) > 0)
                    {
                        $aT = explode(",", $aArgs[0][1]);
                        foreach ($aT as $arg)
                        {
                            $a3 = explode("=", trim($arg));
                            $aAllArgs[] = [
                                'param' => $a3[0],
                                'default'   => $a3[1]
                            ];
                        }
                    }
                    $aTempInfo['params'] = $aAllArgs;
                }

                $this->aBuffer['methods'][] = $aTempInfo;
            }
        }
    }
}
