<?php

declare(strict_types = 1);

namespace display_code;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class DisplayCodeUtilities
{
    /**
     * @return string
     */
    static function getRequestFileName(): string
    {
        $aTemp = explode( DIRECTORY_SEPARATOR,
            (
            $_SERVER["SCRIPT_NAME"] ??
                (
                $_SERVER["PHP_SELF"] ??
                    (
                     $_SERVER["REQUEST_URI"] ?? [""] 
                    )
                )
            )
        );

        $aTemp2 = explode(".", array_pop($aTemp));
        return $aTemp2[0];
    }
}
