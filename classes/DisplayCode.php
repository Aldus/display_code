<?php

declare(strict_types = 1);

namespace display_code;

use LEPTON_abstract;

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

class DisplayCode extends LEPTON_abstract
{
    /**
     *  Internal var to hold the name of the module table name without the TABLE_PREFIX.
     *  @var    string  $tableName
     */
    protected string $tableName = "mod_display_code";
    
    /**
     *  Internal boolean for CodeMirror support. Is CodeMirror available?
     *  @var    boolean $codeMirrorSupport
     */
    public bool $codeMirrorSupport = false;
    
    /**
     *  Var holds a linear list of the internal templates for the frontend-output.
     *  @var    array   $frontendDisplayTemplates
     */
    public array $frontendDisplayTemplates = [];
    
    /**
     *  Internal instance of this class
     *
     */
    static $instance;
    
    public array $allEntries = [];
    public array $allGroups = [];
    public array $groupTree = [];
    
    /**
     *  Initialize the class.
     *
     */
    public function initialize()
    {
        self::$instance->codeMirrorSupport = class_exists( "lib_codemirror", true);
        
        \LEPTON_handle::register("file_list");
        
        $this->frontendDisplayTemplates = file_list(
            dirname(__DIR__)."/templates/frontend",
            ["index.php", "internal_last_edit.lte", "internal_see_also.lte"],
            false,
            "lte",
            dirname(__DIR__)."/templates/frontend/"
        );
        
        $database = \LEPTON_database::getInstance();

        $database->execute_query(
            "SELECT m.`id`, m.`title`, m.`page_id`, m.`section_id`, p.`link` 
                FROM 
                    `" . TABLE_PREFIX . $this->tableName . "` AS m
                JOIN
                    `" . TABLE_PREFIX . "pages` AS p
                WHERE   m.`title` != ''
                AND     p.`page_id` = m.`page_id` 
                ORDER BY m.`title`
            ",
            true,
            $this->allEntries,
            true
        );

        $database->execute_query(
            "SELECT `id`,`name`,`parent`,`position`,`description`,`active` FROM `" . TABLE_PREFIX . $this->tableName . "_groups` ORDER BY `name`,`parent`,`position`",
            true,
            $this->allGroups,
            true
        );
        
        $this->tree();
    }
    
    /**
     *
     *  @param  string  $sSource    The source to be parsed. Pass by reference!
     *  @param  string  $sClassName Additional second classname.
     */
    static public function colorSource(string &$sSource = "", string $sClassName = "", string $sSourceType = "php") {

        switch($sSourceType)
        {
            case "php":
                \display_code\source\Php::getInstance()->colorSource( $sSource, $sClassName );
                $sSource = str_replace("\t", "  ", $sSource);
                
                break;
                
            case "html":
                \display_code\source\Html::getInstance()->colorSource( $sSource, $sClassName );
                break;

            default:
                die("dc [102]: source type not one of php or html.");
        }
    }

    /**
     *
     * @param  string  $aSource    The source to be parsed. Call by reference!
     * @return boolean
     */
    static public function lineNumbers(string &$aSource = ""): bool
    {
        if ($aSource == "") {
            return false;
        }

        $a = explode("\n", $aSource);
        $s = "<ol class='ln'>";
        foreach ($a as $line) {
            $s .= "<li>" . $line . "</li>";
        }
        $s .= "</ol>";

        $aSource = $s;

        return true;
    }

    private function tree()
    {
        $this->dcBuildGroupTree(
            0,
            $this->groupTree
        );
    
    }

    /**
     *  Internal private method for the tree.
     *
     *  @param  integer $iParentID  A valid parent-id >= 0
     *  @param  array   $aStorage   An array to store the values. Call by reference.
     */
    private function dcBuildGroupTree(int $iParentID = 0, array &$aStorage = [])
    {
        $aStorage['subgroup'] = [];
        
        foreach($this->allGroups as &$ref)
        {
            if($ref['parent'] == $iParentID)
            {
                $this->dcBuildGroupTree( $ref['id'], $ref );
                $aStorage['subgroup'][] = $ref;
            }    
        }
    }
}

