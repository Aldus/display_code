<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
// [1]
$aTableInfo = array();
$database->describe_table(
    TABLE_PREFIX."mod_display_code",
    $aTableInfo
);

$bCaptureFound = false;
$bGroupFound = false;
$bPositionFound = false;

const ALTER_TABLE_PREFIX = "ALTER TABLE";

foreach($aTableInfo as $field)
{
    if ($field["Field"] == "sorce_type")
    {
        $database->simple_query(ALTER_TABLE_PREFIX . " `".TABLE_PREFIX."mod_display_code` CHANGE `sorce_type` `source_type` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT 'php' ");
    }
    if ($field["Field"] == "caption")
    {
        $bCaptureFound = true;
    }
    if ($field["Field"] == "group")
    {
        $bGroupFound = true;
    }
    if ($field["Field"] == "position")
    {
        $bPositionFound = true;
    }
}

if (false === $bCaptureFound)
{
    $database->simple_query(ALTER_TABLE_PREFIX . " `".TABLE_PREFIX."mod_display_code` ADD `caption` TEXT  NULL  AFTER `template`");
}

if (false === $bGroupFound)
{
    $database->simple_query(ALTER_TABLE_PREFIX . " `".TABLE_PREFIX."mod_display_code` ADD `group` INT  NULL  DEFAULT '0'  AFTER `caption`");
}

if (false === $bPositionFound)
{
    $database->simple_query(ALTER_TABLE_PREFIX . " `".TABLE_PREFIX."mod_display_code` ADD `position` INT  NULL  DEFAULT '1'  AFTER `group`;");
}

$database->simple_query(ALTER_TABLE_PREFIX . " `".TABLE_PREFIX."mod_display_code` CHANGE `description` `description` TEXT  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL;");

// [2]
$strip = TABLE_PREFIX;
$allTables = $database->list_tables( $strip );

if (!in_array("mod_display_code_groups", $allTables))
{
    $query ="CREATE TABLE `".TABLE_PREFIX."mod_display_code_groups` (
        `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
        `name`          varchar(255) DEFAULT 'No name',
        `description`   text,
        `active`        int(11) DEFAULT '1',
        `parent`        int(11) DEFAULT '0',
        `position`      int(11) DEFAULT '1',
        PRIMARY KEY (`id`) );
    ";

    if (false === $database->simple_query( $query ) )
    {
        $admin->print_error( $database->get_error() );
    }
}

echo LEPTON_tools::display("Upgrade successfully.", "pre", "ui message green");
