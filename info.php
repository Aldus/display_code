<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$module_directory   = 'display_code';
$module_name        = 'Display Code';
$module_function    = 'page';
$module_version     = '0.5.2.2';
$module_platform    = '7.0.0';
$module_delete      =  true;
$module_author      = 'Dietrich Roland Pehlke';
$module_license     = 'cc 3.0 by-sa';
$module_license_terms   = 'https://creativecommons.org/licenses/by/3.0/';
$module_description     = 'Just a private module for display some source-code (e.g. for documentation and examples).';
$module_guid        = "F956F66D-6278-440C-93B4-AD6EFD1615E8";
