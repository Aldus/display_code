<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){ define("SEC_FILE",'/framework/class.secure.php' ); }
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneBack = "../";
    $root = $oneBack;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneBack;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include class.secure.php
