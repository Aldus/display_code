<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$admin = LEPTON_admin::getInstance( "Pages", "start", false, false );

// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

$database = LEPTON_database::getInstance();

/**
 *	Update the display_code table with the changes
 *
 */
if(isset($_POST["names"]))
{
    if(isset($_POST['delete']) && is_array($_POST['delete']) )
    {
        foreach($_POST['delete'] as $id=>$val)
        {
            $database->simple_query("DELETE from `".TABLE_PREFIX."mod_display_code_groups` WHERE `id`=".intval($id));
        }
    }
    
    if(is_array($_POST['names']))
    {
        foreach($_POST['names'] as $id=>$name)
        {
    
            $id = intval($id);
            $fields = [
                'name' => $name,
                'description'   => $_POST['descriptions'][$id],
                'parent'        => $_POST['parents'][$id],
                'active'        => intval(($_POST['actives'][$id] ?? 0)),
                'position'      => $_POST['positions'][$id]
            ];
 
            $database->build_and_execute(
                "update",
                TABLE_PREFIX."mod_display_code_groups",
                $fields,
                "`id`=".$id
            );
        }
    }
}

$edit_page = LEPTON_URL."/modules/display_code/modify_groups.php?page_id=".$page_id.'#'.SEC_ANCHOR.$section_id;

// Check if there is a database error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), $edit_page );
} else {
	$admin->print_success($MESSAGE['PAGES_SAVED'], $edit_page );
}

// Print admin footer
$admin->print_footer();
