<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$oDisplayCode = \display_code\DisplayCode::getInstance();

/**
 *	Get the values for this section from the DB
 */
$vars = array();
LEPTON_database::getInstance()->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_display_code` WHERE `section_id`=".$section_id,
	true,
	$vars,
	false
);

/**
 *	Temp. hash
 */
$hash = hash("sha512", (string) time());

$form_values = array(
	'form_action'	=> LEPTON_URL."/modules/display_code/save.php",
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'leptoken'		=> get_leptoken(),
	'oDC'	        => $oDisplayCode,
	'vars'			=> $vars,
	'hash'			=> $hash
);

$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule( "display_code");

echo $oTwig->render( 
	"@display_code/modify.lte",	//	template-filename
	$form_values	//	template-data
);

