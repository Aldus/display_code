<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$admin = LEPTON_admin::getInstance();
$database = LEPTON_database::getInstance();

$fields = [
    'name'          => "no group name",
    'description'   => "",
    'position'      => 1,
    'parent'        => 0
];

$database->build_and_execute(
    "insert",
    TABLE_PREFIX."mod_display_code_groups",
    $fields
);

if ($database->is_error())
{
    trigger_error(sprintf('[%s - %s] %s', __FILE__, __LINE__, $database->get_error()), E_USER_ERROR);
} else {

    $page_id = $_GET["page_id"] ?? 0;
    $section_id = $_GET["section_id"] ?? 0;
    
    $admin->print_success(
        $MESSAGE['PAGES_SAVED'],
        LEPTON_URL."/modules/display_code/modify_groups.php?page_id=".$page_id."&section_id=".$section_id
    );
}
