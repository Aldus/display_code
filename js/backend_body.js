/**
 *	Display code
 *	Backend JavaScript (after the end-body tag)
 *
 */

function send_display_code_form( aRef ) {
    return true;	
}

function display_message( aText, aID, aStatus, aStatusText ) {

	let temp = document.getElementById("message_box_"+aID);

	if(temp) {
		temp.style.visibility="visible";
		temp.style.display="block";
		
		if(aStatus != 200){
			temp.setAttribute( "class", "display_code_message display_code_error");
			aText = "<h4>Error: "+aStatus+"</h4><p>"+aStatusText+"</p><p></p>"+aText;
		}
		
		temp.innerHTML = aText;
		build_close_button( temp );
	}
}

function build_close_button( aRef ) {

	let element = document.createElement("input");
	element.setAttribute("type", "button");
	element.setAttribute("onclick", "this.parentElement.style.visibility='hidden';this.parentElement.style.display='none';");
	element.setAttribute("style", "width: 120px;margin-bottom:20px;");
	
	element.setAttribute("value", "ok");
	
	aRef.innerHTML += "<p></p>"; 
	aRef.appendChild( element );

}