<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$admin = LEPTON_admin::getInstance();
/**
 *	Update the display_code table with the changes
 *
 */
if(isset($_POST["title"])) {

	$oValidator = new LEPTON_request();

	$fields = array(
		"title"			=> $oValidator->get_request("title",        "(ald)",    "string"),
		"description"	=> $oValidator->get_request("description",  "",         "string"),
		"see_also"		=> implode( ",", ($_POST['see_also'] ?? array())),
		"source_type"	=> $oValidator->get_request("source_type",  "php",      "string"),
		"source"		=> $oValidator->get_request("source",       "",         "string"),
		"last_edit"		=> time(),
		"active"		=> $oValidator->get_request("active",       0,          "integer"),
		"html_type"		=> $oValidator->get_request("html_type",    "pre",      "string"),
		"style"			=> $oValidator->get_request("style",        "default",  "string"),
		"line_numbers"	=> $oValidator->get_request("line_numbers", 0,          "integer"),
	#	"line_start"	=> 1,
		"template"		=>  $oValidator->get_request("template",    "view.lte", "string"),
		"caption"		=>  $oValidator->get_request("caption",     "",         "string"),
		"group"		    => $oValidator->get_request("group",        0,          "integer")
	);

	LEPTON_database::getInstance()->build_and_execute(
		"update",
		TABLE_PREFIX."mod_display_code",
		$fields,
		"`section_id`=".intval( $_POST['section_id'] )
	);

}

$edit_page = ADMIN_URL."/pages/modify.php?page_id=".$page_id.'#'.SEC_ANCHOR.$section_id;

// Check if there is a database error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), $edit_page );
} else {
	$admin->print_success($MESSAGE['PAGES_SAVED'], $edit_page );
}

// Print admin footer
$admin->print_footer();

?>