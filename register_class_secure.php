<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

$files_to_register = [
	"add.php",
	"delete.php",
	"modify.php",
	"save.php",
	"modify_groups.php",
	"save_groups.php",
	"add_group.php"
];

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
