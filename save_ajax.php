<?php

declare(strict_types = 1);

if(!isset($_POST['section_id'])) {
    die();
}

require_once( "../../config/config.php");
require_once( "../../framework/initialize.php");

$oValidate = new LEPTON_request();

$fields = array(
	'title'			=> $oValidate->get_request('title', '', 'string'),
	'description'	=> $oValidate->get_request('description', '', 'string'),
	'style'			=> $oValidate->get_request('style', 'default', 'string'),
	'template'		=> $oValidate->get_request('template', 'view.lte', 'string'),
	'source'		=> $oValidate->get_request('source', '', 'string')
);

$bJobOk = $database->build_and_execute(
	'update',
	TABLE_PREFIX."mod_display_code",
	$fields,
	"`section_id`=".intval( $_POST['section_id'] )
);

if(false === $bJobOk) {
	echo "error: ".$database->get_error();
} else {
	echo "saved!";
}
