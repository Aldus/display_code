<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
if(!isset($_GET['page_id']))
{
    $_GET['page_id'] = ($_POST['page_id'] ?? 0);
}

$admin = LEPTON_admin::getInstance();

$oDisplayCode = \display_code\DisplayCode::getInstance();

/**
 *	Temp. hash
 */
$hash = hash("sha512", (string) time());

$form_values = array(
	"form_action"	=> LEPTON_URL."/modules/display_code/save_groups.php",
	"section_id"	=> $section_id,
	"page_id"		=> $page_id,
	"leptoken"		=> get_leptoken(),
	"oDC"	        => $oDisplayCode,
	"hash"			=> $hash
);

$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule("display_code");

echo $oTwig->render( 
	"@display_code/modify_groups.lte",
	$form_values
);

$admin->print_footer();
