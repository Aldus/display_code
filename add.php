<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file 
global $database, $section_id, $page_id;

// Add a new WYSIWYG record
$fields = array(
    'page_id'		=> $page_id,
    'section_id'	=> $section_id,
    'title'			=> "",
    'description'	=> "",
    'see_also'		=> "",
    'source_type'	=> "php",
    'source'		=> "// your php-code here\n",
    'last_edit'		=> time(),
    'active'		=> 1,
    'html_type'		=> "pre",
    'style'			=> "default",
    'line_numbers'	=> 0,
    'line_start'	=> 1,
    'template'		=> "box.lte"
);

$database->build_and_execute(
    "insert",
    TABLE_PREFIX."mod_display_code",
    $fields
);

if ($database->is_error())
{
    trigger_error(sprintf('[%s - %s] %s', __FILE__, __LINE__, $database->get_error()), E_USER_ERROR);
}
