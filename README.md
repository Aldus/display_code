## Display Code

A page-module for [LEPTON CMS][1].

### Preface
This is just my private playground. The goal is to map the source code for frontend.

#### License
[cc 3.0 by-sa][2]

#### Requirements
* PHP 8.0 recommended
* [LEPTON CMS][1] (5.2 recommended)

###### temp

```code
$oDC = display_code_parser::getInstance();

$oDC->getFile( LEPTON_PATH."/framework/classes/lepton_tools.php");

echo LEPTON_tools::display( $oDC->aBuffer );
```

###### Example screenshoot
![screenshoot](/img/dc_screen.png)

##### Droplets
###### 1: dcDisplayGroup
```code
$id = ($id ?? 0);
return display_code_frontend::displayGroupByID( $id );
```
usage:
```Code
[[dcDisplayGroup?id=2]]
```

###### 2: dcDisplaySection
```Code
$id = ($id ?? NULL);
return display_code_frontend::displayCodeBySection( $id );
```
usage:
```Code
[[dcDisplaySection?id=81]]
```

###### 3: dcDisplayById
```Code
$id = ($id ?? NULL);
return display_code_frontend::displayCodeByID( $id );
```
usage:
```Code
[[dcDisplayById?id=81]]
```

---
[1]: http://lepton-cms.org "LEPTON CMS"
[2]: https://creativecommons.org/licenses/by-sa/3.0/de/ "cc 3.0 by-sa"
