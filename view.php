<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){ define("SEC_FILE",'/framework/class.secure.php' ); }
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneBack = "../";
    $root = $oneBack;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneBack;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include class.secure.php

/**
 *	Get the values for this section from the DB
 */
$vars = [];

LEPTON_database::getInstance()->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_display_code` WHERE `section_id`=" . $section_id,
	true,
	$vars,
	false
);

$style = ($vars['style'] === "default") ? "" : $vars['style'];

if ($vars['source_type'] === "html")
{
    $vars['source'] = str_replace("<", "&lt;", $vars['source']);
}

\display_code\DisplayCode::colorSource( $vars['source'] , $style, $vars['source_type']);

if ($vars['line_numbers'] === 1)
{
	\display_code\DisplayCode::linenumbers( $vars['source'] );
}

$oDISPLAY_CODE = \display_code\DisplayCode::getInstance();
$aSeeAlso = [];
$aTempList = explode( ",", $vars['see_also'] );
$aTempList = array_map(function($a){ return (int)$a;}, $aTempList);
foreach ($aTempList as $iTempId)
{
    foreach ($oDISPLAY_CODE->allEntries as $ref)
    {
        if ($ref['id'] === $iTempId)
        {
            $link = LEPTON_URL.PAGES_DIRECTORY.$ref['link'].".php#".SEC_ANCHOR.$ref['section_id'];
            
            $aSeeAlso[] = [
                'title' => $ref['title'],
                'link'  => $link
            ];
            break;
        }
    }
}

/**
 *	Collect the output values here
 */
$page_values = [
    'content'   => $vars,
    'see_also'  => $aSeeAlso,
    'language'  => &$oDISPLAY_CODE->language
];

$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule("display_code");

echo $oTwig->render( 
    "@display_code/frontend/".$vars['template'],
    $page_values
);
