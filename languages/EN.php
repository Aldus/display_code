<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


$MOD_DISPLAY_CODE = [
    "hello"         => "Display sourcecode",
    "title"         => "Title",
    "description"   => "Description",
    "see_also"      => "See also",
    "source_type"   => "Language",
    "source"        => "Source",
    "active"        => "Active?",
    "html_type"     => "HTML type",
    "style"         => "Display style",
    "template"      => "Template",
    "linenumbers"   => "Line numbers",
    "caption"       => "Caption",
    "last_edit"     => "last edit",
    "last_edit_format"  => "d. M Y T H:i:s",
    "group"         => "Group",
    "no_group"      => "no group",
    "position"      => "Position",
    "parent"        => "Parent",
    "no_parent"     => "no parent",
    "cancel_and_back"   => "Back",
    "no_section_found"  => "No section found for this section-id! [%s]",
    "no_id_match"       => "No entries found for this id! [%s]",
    "page_label"        => "Page: ",
    "new_group"           => "New group"
];
