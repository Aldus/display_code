<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


$MOD_DISPLAY_CODE = [
    "hello"         => "Meine private <em>»Bau-Umgebung«</em>.",
    "title"         => "Titel",
    "description"   => "Beschreibung",
    "see_also"      => "Siehe auch",
    "source_type"   => "Sprache/Typ",
    "source"        => "Quellcode",
    "active"        => "Aktiv?",
    "html_type"     => "HTML Typ",
    "style"         => "Darstellung",
    "template"      => "Template/Vorlage",
    "linenumbers"   => "Linien Nummerierung",
    "caption"       => "Unterzeile",
    "last_edit"     => "Letzte Änderung am",
    "last_edit_format"  => "j.m.Y \u\m H:i \U\h\\r", // be aware for the backslshes here!
    "group"         => "Gruppe",
    "no_group"      => "<em>keine</em>",
    "position"      => "Position",
    "parent"        => "Übergeordnete Gruppe",
    "no_parent"     => "keine",
    "cancel_and_back"   => "Zurück",
    "no_section_found"  => "Zu der angegebenen ID wurde keine Sektion gefunden! [%s]",
    "no_id_match"       => "Keine Einträge zur id %s gefunden!",
    "page_label"        => "Seite: ",
    "new_group"           => "Neu *"
];
