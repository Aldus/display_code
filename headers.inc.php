<?php

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
$mod_headers = [
    'backend' => [
        'css' => [
            [
                'media' => 'all',
                'file' => 'modules/display_code/css/backend.css'
            ]
        ]
    ]
];

if (("algos" === DEFAULT_THEME) && (\display_code\DisplayCodeUtilities::getRequestFileName() === "modify_groups"))
{
    $mod_headers['backend']['js'] = [
        "modules/lib_jquery/jquery-ui/jquery-ui.min.js",
        "modules/lib_semantic/dist/semantic.min.js"
    ];

    $mod_headers['backend']['css'][] = [
        'media' => "all",
        'file'  => "modules/lib_jquery/jquery-ui/jquery-ui.min.css"
    ];

    $mod_headers['backend']['css'][] = [
        'media' => "all",
        'file'  => "modules/lib_semantic/dist/semantic.min.css"
    ];
}

// -- [2]
if (true === \display_code\DisplayCode::getInstance()->codeMirrorSupport)
{
    $oCodeMirror = lib_codemirror::getInstance();
    
    $mod_headers['backend']['css'][] = array(
        "media" => "all",
        "file"  => "/modules/lib_codemirror/css/backend.css"
    );
    
    $oCodeMirror->useFavorites = true;
    $aCodeMirrorFiles = $oCodeMirror->getBaseFiles();

    foreach($aCodeMirrorFiles['css'] as $sCssPath)
    {
        $mod_headers['backend']['css'][] = array(
            'media'  => 'all',
            'file'  => $sCssPath
        );
    }

    $mod_headers['backend']['css'][] = array(
        "media" => "all",
        "file"  => "/modules/lib_codemirror/CodeMirror/addon/scroll/simplescrollbars.css"
    );
    
    // js
    foreach($aCodeMirrorFiles['js'] as $sJSPath)
    {
        $mod_headers['backend']['js'][] = $sJSPath;
    }
    
    $mod_headers['backend']['js'][] = "/modules/lib_codemirror/CodeMirror/addon/scroll/simplescrollbars.js";
}
