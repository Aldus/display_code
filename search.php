<?php

declare(strict_types = 1);

/**
 *  Display code
 *
 *  Private L* VII project
 *
 *  @package    LEPTON-CMS modules
 *  @module     DisplayCode
 *  @author     Dietrich Roland Pehlke
 *  @license    cc 3.0 by-sa *
 *
 */

function display_code_search($func_vars) {
	extract($func_vars, EXTR_PREFIX_ALL, 'func');
	
	// how many lines of excerpt we want to have at most
	$max_excerpt_num = $func_default_max_excerpt;
	$divider = ".";
	$result = false;
	
	$table = TABLE_PREFIX."mod_display_code";

	$query = [];
	$func_database->execute_query("
		SELECT `description`,`title`,`source`		FROM `".$table."`
		WHERE `section_id`=".$func_section_id."
	    ",
	    true,
	    $query,
	    false
	);

	if(count($query) > 0) {
		$res = $query; // 1
        $mod_vars = array(
            'page_link' => $func_page_link,
            'page_link_target' => "#wb_section_$func_section_id",
            'page_title' => $func_page_title,
            'page_description' => $func_page_description,
            'page_modified_when' => $func_page_modified_when,
            'page_modified_by' => $func_page_modified_by,
            'text' => $res['description'].$divider.$res['title'].$divider.$res['source'],
            'max_excerpt_num' => $max_excerpt_num
        );
		if(print_excerpt2($mod_vars, $func_vars)) {
		    $result = true;
        }
	}
	return $result;
}
